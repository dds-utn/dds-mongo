package ar.edu.utn.dds;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("usuarios")
public class Usuario {
	@Id
	private ObjectId id;
	
	
	private String nombre;
	private Integer edad;
	private List<String> no_puede;
	public ObjectId getId() {
		return id;
	}
	public void setId(ObjectId id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public List<String> getNo_puede() {
		return no_puede;
	}
	public void setNo_puede(List<String> no_puede) {
		this.no_puede = no_puede;
	}
	
	
	
}
