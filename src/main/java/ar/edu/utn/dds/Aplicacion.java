package ar.edu.utn.dds;


import static spark.Spark.get;
import static spark.Spark.port;
import static spark.debug.DebugScreen.enableDebugScreen;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Criteria;
import org.mongodb.morphia.query.Query;

import com.mongodb.MongoClient;

public class Aplicacion {

	
	
	public static void main(String[] args) {
		port(4567);
		MongoClient mongoClient = new MongoClient();
		
		enableDebugScreen();
		final Morphia morphia = new Morphia();
		morphia.mapPackage("ar.edu.utn.dds");		
		final Datastore datastore = morphia.createDatastore(mongoClient, "dds");
		get("/", (req,res) -> {
			Query<Usuario> query = datastore.createQuery(Usuario.class);
			if(req.queryParams("edad") != null ){
				query.field("edad").greaterThanOrEq( Integer.parseInt( req.queryParams("edad").toString()  ));
			}
			return query.asList();
		},new JsonTransformer());
		
		
		
	}

}
