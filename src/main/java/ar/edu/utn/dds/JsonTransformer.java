package ar.edu.utn.dds;

import java.io.StringWriter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import spark.ResponseTransformer;

public class JsonTransformer implements ResponseTransformer {

	private ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public String render(Object data) throws Exception {
		
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		StringWriter sw = new StringWriter();
		mapper.writeValue(sw, data);
		return sw.toString();
	}

}
